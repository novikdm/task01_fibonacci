/**
 * @(#)Main.java
 *
 * Copyright (c) 2019 NovikDM
 *
 * This software is designed for training purposes
 */
package ua.com.novikdm;

import java.util.Scanner;

/**
 * <h1>Fibonacci Numbers</h1>
 * The Fibonacci Numbers program implements
 * some methods of calculate and represents Fibonacci Numbers.
 * <p>
 *   Future list:
 * <ol>
 *   <li>Set start and end index of Fibonacci numbers</li>
 *   <li>Print odd and even numbers of Fibonacci numbers</li>
 *   <li>Print the sum of odd and even numbers</li>
 *   <li>Print the biggest odd number and the biggest even number</li>
 *   <li>Print percentage of odd and even Fibonacci numbers</li>
 * </ol>
 * </p>
 *
 * @author Dmitro Novik
 * @version 1.0
 * @since  2019-03-31
 */
public class Main {

  /**
   * Initialize Fibonacci numbers array.
   */
  private static FibonacciNumbersArray fibonacciNumbersArray =
          FibonacciNumbersArray.getInstance();

  /**
   * Start program method.
   * This method print welcome massage and
   * call main menu.
   * @param args not use in this program.
   */
  public static void main(String[] args) {
    printSeparator();
    String welcomeString =
        "--------------Welcome to Fibonacci Numbers Program!--------------";
    System.out.println(welcomeString);
    printSeparator();
    System.out.println();
    runMainMenu();
  }

  /**
   * This method print main menu.
   * User can write number and program call specified method.
   */
  private static void runMainMenu() {
    Scanner scanner = new Scanner(System.in);
    boolean whileTrigger = true;
    while (whileTrigger) {
      printMenu();
      switch (scanner.nextInt()) {
        case 1:
          setInterval();
          exitToMainMenu();
          break;
        case 2:
          printFibonacciNumbers();
          exitToMainMenu();
          break;
        case 3:
          printSumOfOddAndEvenNumbers();
          exitToMainMenu();
          break;
        case 4:
          printTheBiggestOddAndEvenNumbers();
          exitToMainMenu();
          break;
        case 5:
          printPercentageOfOddAndEvenNumbers();
          exitToMainMenu();
          break;
        case 0:
          whileTrigger = false;
          break;
        default:
          System.out.println("Error! Try again!");
          printSeparator();
      }
    }
  }

  /**
   * Main Menu methods.
   */

  /**
   * This method is used to set
   * start and end index of Fibonacci numbers.
   */
  private static void setInterval() {
    System.out.println("Write left index of interval");
    Scanner scanner = new Scanner(System.in);
    int leftBorder = scanner.nextInt();
    fibonacciNumbersArray.setStartOfFibonacciNumbersArray(leftBorder);
    System.out.println("Write right index of interval");
    int rightBorder = scanner.nextInt();
    fibonacciNumbersArray.setEndOfFibonacciNumbersArray(rightBorder);

  }

  /**
   * This method is used to print
   * odd and even numbers of Fibonacci numbers.
   */
  private static void printFibonacciNumbers() {
    fibonacciNumbersArray.printFibonacciNumbers();
  }

  /**
   * This method is used to print
   * the sum of odd and even numbers.
   */
  private static void printSumOfOddAndEvenNumbers() {
    fibonacciNumbersArray.printSumOfOddAndEvenFibonacciNumbers();
  }

  /**
   * This method is used to print
   * the biggest odd number and the biggest even number.
   */
  private static void printTheBiggestOddAndEvenNumbers() {
    fibonacciNumbersArray.printBiggestOddAndEvenFibonacciNumbers();
  }

  /**
   * This method is used to print
   * Print percentage of odd and even Fibonacci numbers.
   */
  private static void printPercentageOfOddAndEvenNumbers() {
    fibonacciNumbersArray.printPercentageOfOddAndEvenNumbers();
  }


  /**
   * Auxiliary methods.
   */

  /**
   * This method is used to print Main Menu.
   */
  private static void printMenu() {
    printSeparator();
    System.out.println("MENU:");
    System.out.println("1 - Enter the interval of Fibonacci numbers");
    System.out.println("2 - Print Fibonacci numbers");
    System.out.println("3 - Print the sum of odd and even numbers");
    System.out.println(
            "4 - Print the biggest odd number and the biggest even number"
    );
    System.out.println(
            "5 - Print percentage of odd and even Fibonacci numbers"
    );
    System.out.println("0 - Exit");
    System.out.println();
    System.out.println("Current interval: ["
        + (fibonacciNumbersArray.getStartOfFibonacciNumbersArray() + 1)
        + ";"
        + fibonacciNumbersArray.getEndOfFibonacciNumbersArray() + "]");
    printSeparator();
  }

  /**
   * This method is used to exit in Main Menu.
   */
  private static void exitToMainMenu() {
    Scanner scanner = new Scanner(System.in);
    while (true) {
      System.out.println("0 - to Main Menu");
      if (scanner.nextInt() != 0) {
        System.out.println("Error! Try again!");
      } else {
        break;
      }
    }
  }

  /**
   * This method is used to print line separator.
   */
  private static void printSeparator() {
    String separator =
        "-----------------------------------------------------------------";
    System.out.println(separator);
  }

}
