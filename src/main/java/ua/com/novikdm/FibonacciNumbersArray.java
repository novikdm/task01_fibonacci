/**
 * @(#)FibonacciNumbersArray.java
 *
 * Copyright (c) 2019 NovikDM
 *
 * This software is designed for training purposes
 */
package ua.com.novikdm;

import java.util.ArrayList;

/**
 * <h1>Fibonacci Numbers Array</h1>
 * This class realize array of N Fibonacci numbers as Singleton.
 * Class contains methods, witch:
 * calculate Fibonacci numbers,
 * print odd and even numbers of Fibonacci numbers,
 * calculate percentage of odd and even numbers,
 * calculate sum of odd and even numbers.
 *
 * @author Dmitro Novik
 * @version 1.0
 * @since  2019-03-31
 */
public final class FibonacciNumbersArray {

  /**
   * Object of Fibonacci numbers.
   */
  private static FibonacciNumbersArray fibonacciNumbersArray =
          new FibonacciNumbersArray();

  /**
   * Start index of Fibonacci numbers.
   * 0 - default start index
   */
  private int startOfFibonacciNumbersArray = 0;

  /**
   * End index of Fibonacci numbers.
   * 92 - default end index
   */
  private int endOfFibonacciNumbersArray = 92;

  /**
   * Fibonacci numbers List.
   */
  private ArrayList<Long> fibonacciNumbersArrayList;

  /**
   * This method is used to @return single Object FibonacciNumbersArray.
   */
  public static FibonacciNumbersArray getInstance() {
    return fibonacciNumbersArray;
  }
  /**
   * This method is used to initialization of FibonacciNumbersArray.
   */
  private FibonacciNumbersArray() {
    fibonacciNumbersArrayList = calculateFibonacciNumbersArray();
  }

  /**
   * Getters methods.
   */

  /**
   * @return starting index of Fibonacci List.
   */
  public int getStartOfFibonacciNumbersArray() {
    return startOfFibonacciNumbersArray;
  }

  /**
   * @return ending index of Fibonacci List.
   */
  public int getEndOfFibonacciNumbersArray() {
    return endOfFibonacciNumbersArray;
  }

  /**
   * Setters methods.
   * These methods are used to set left and right border
   * of Fibonacci numbers indexes
   */
  /**
   * This method set start index of Fibonacci numbers List.
   * From
   * @param startOfFibonacciNumbersArray
   */
  public void setStartOfFibonacciNumbersArray(
          int startOfFibonacciNumbersArray) {
    if (startOfFibonacciNumbersArray > 0) {
      this.startOfFibonacciNumbersArray = startOfFibonacciNumbersArray - 1;
    } else {
      System.out.println("Wrong number!");
    }
  }

  /**
   * This method set end index of Fibonacci numbers List.
   * From
   * @param endOfFibonacciNumbersArray
   */
  public void setEndOfFibonacciNumbersArray(int endOfFibonacciNumbersArray) {
    if (endOfFibonacciNumbersArray > 0) {
      this.endOfFibonacciNumbersArray = endOfFibonacciNumbersArray;
      fibonacciNumbersArrayList = calculateFibonacciNumbersArray();
    } else {
      System.out.println("Wrong Number!");
    }
  }

  /**
   * Methods for external use.
   */

  /**
   * This method print odd numbers of Fibonacci array
   * from start to end and even from end to start.
   */
  public void printFibonacciNumbers() {
    System.out.println("Fibonacci numbers:");
    printList(fibonacciNumbersArrayList);
    System.out.println("Odd numbers from start to end of interval:");
    printList(getOddNumbers());
    System.out.println("Even numbers from end to start of interval:");
    printList(getEvenNumbers());
  }

  /**
   * This method print the sum of odd numbers and
   * the sum of even numbers from Fibonacci array.
   */
  public void printSumOfOddAndEvenFibonacciNumbers() {
    System.out.print("Sum of odd numbers: ");
    System.out.println(getSumOfNumbers(getOddNumbers()));
    System.out.print("Sum of even numbers: ");
    System.out.println(getSumOfNumbers(getEvenNumbers()));
  }

  /**
   * This method print the biggest of odd and even numbers
   * in Fibonacci array.
   */
  public void printBiggestOddAndEvenFibonacciNumbers() {
    System.out.print("The biggest odd number: ");
    System.out.println(getBiggestNumber(getOddNumbers()));
    System.out.print("The biggest even number: ");
    System.out.println(getBiggestNumber(getEvenNumbers()));
  }

  /**
   * This method print percentage of odd end even numbers
   * in Fibonacci array.
   */
  public void printPercentageOfOddAndEvenNumbers() {
    int quantityOfOddNumbers = getOddNumbers().size();
    int allNumbers = fibonacciNumbersArrayList.size();
    double percentageOfOddNumbers =
        Math.round((quantityOfOddNumbers * 100.0) / allNumbers);
    double percentageOfEvenNumbers =
        Math.round(100 - percentageOfOddNumbers);
    System.out.println("Odd numbers: " + percentageOfOddNumbers + "%");
    System.out.println("Even numbers: " + percentageOfEvenNumbers + "%");
  }

  /**
   * Methods for internal use.
   */

  /**
   * This method calculate Fibonacci numbers from
   * startOfFibonacciNumbersArray to endOfFibonacciNumbersArray.
   * And @return ArrayList of Fibonacci numbers
   */
  private ArrayList<Long> calculateFibonacciNumbersArray() {
    ArrayList<Long> arrayOfFibonacciNumbers = new ArrayList<Long>();
    ArrayList<Long> tempArrayOfFibonacciNumbers = new ArrayList<Long>();
    for (int i = 0; i < endOfFibonacciNumbersArray; i++) {
      if (i == 0 || i == 1) {
        tempArrayOfFibonacciNumbers.add(i, 1L);
      } else {
        tempArrayOfFibonacciNumbers.add(
            tempArrayOfFibonacciNumbers.get(i - 1)
            + tempArrayOfFibonacciNumbers.get(i - 2));
      }
    }
    for (int i = startOfFibonacciNumbersArray;
         i < tempArrayOfFibonacciNumbers.size();
         i++) {
      arrayOfFibonacciNumbers.add(tempArrayOfFibonacciNumbers.get(i));
    }
    return arrayOfFibonacciNumbers;
  }

  /**
   * This method get odd numbers from
   * List of Fibonacci numbers - fibonacciNumbersArrayList
   * and @return ArrayList of odd Fibonacci numbers.
   */
  private ArrayList<Long> getOddNumbers() {
    ArrayList<Long> oddNumbers = new ArrayList<Long>();
    for (int i = 0; i < fibonacciNumbersArrayList.size(); i++) {
      if (fibonacciNumbersArrayList.get(i) % 2 == 1) {
        oddNumbers.add(fibonacciNumbersArrayList.get(i));
      }
    }
    return oddNumbers;
  }

  /**
   * This method get even numbers from
   * List of Fibonacci numbers - fibonacciNumbersArrayList
   * and @return ArrayList of even Fibonacci numbers.
   */
  private ArrayList<Long> getEvenNumbers() {
    ArrayList<Long> evenNumbers = new ArrayList<Long>();
    for (int i = fibonacciNumbersArrayList.size() - 1; i >= 0; i--) {
      if (fibonacciNumbersArrayList.get(i) % 2 == 0) {
        evenNumbers.add(fibonacciNumbersArrayList.get(i));
      }
    }
    return evenNumbers;
  }

  /**
   * This method find and @return the biggest number.
   * From
   * @param listOfNumbers
   */
  private long getBiggestNumber(ArrayList<Long> listOfNumbers) {
    long biggestNumber = listOfNumbers.get(0);
    for (long number
            : listOfNumbers) {
      if (number > biggestNumber) {
        biggestNumber = number;
      }
    }
    return biggestNumber;
  }

  /**
   * This method calculate and @return sum of numbers from.
   * @param listOfNumbers
   */
  private long getSumOfNumbers(ArrayList<Long> listOfNumbers) {
    long sumOfNumbers = 0;
    for (long number
            : listOfNumbers) {
      sumOfNumbers += number;
    }
    return sumOfNumbers;
  }

  /**
   * This method print.
   *@param listOfNumbers in console
   */
  private void printList(ArrayList<Long> listOfNumbers) {
    for (Long number
            : listOfNumbers) {
      System.out.print(number);
      System.out.print("; ");
    }
    System.out.println();
  }
}
