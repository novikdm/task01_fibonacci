/**
 * @(#)package-info.java
 *
 * Copyright (c) 2019 NovikDM
 *
 * This software is designed for training purposes.
 * @author Dmitro Novik
 * @version 1.0
 * @since  2019-03-31
 */
package ua.com.novikdm;
